const {createApp} = Vue;
createApp({
    data()
    {
        return{
            display:"0",
            operandoAtual:null,
            operandoAnterior:null,
            operador:null,
        };//Fechamento do return
    },//Fechamento da função "data"

    methods:{
        handleButtonClick(botao){
            switch(botao){
                case "+":
                case "-":
                case "/":
                case "*":
                    this.handleOperation(botao);
                    break;

                default:
                    this.handleNumber(botao);
                    break;
            }//Fechamento switch
        },//Fechamento handleButtonClick

        handleNumber(numero){
            if(this.display === "0"){
                this.display = numero.toString();
            }
            else{
                this.display += numero.toString();
                /*this.display = this.display + numero.toString();*/
            }
        },//Fechamento handleNumber

        handleOperation(operacao){
            this.operador = operacao;

            this.operandoAtual = parseFloat(this.display);
            this.display = "0";

        },//Fechamento handleOperation

    },//Fim methods

}).mount("#app"); //Fechamento do "createApp"