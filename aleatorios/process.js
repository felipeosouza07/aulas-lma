const {createApp} = Vue;
createApp({
    data(){
        return {
            randomIndex: 0,
            randomIndexInternet: 0,

            //Vetor de imagens locais
            imagensLocais:[
                './Imagens/peixe.jpg',
                './Imagens/cassio.jpg',
                './Imagens/cachorrofeio.jpg'
            ],

            imagensInternet:[
                'https://i.pinimg.com/236x/47/cb/8a/47cb8a872ecf8280d810acb3565b619e--funny-monkey-pictures-animal-pictures.jpg',
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTh_be_SUoOblSGcTB0d59zJSUdFdXJ-w6teg&usqp=CAU',
                'https://img.estadao.com.br/fotos/crop/1200x1200/resources/jpg/4/6/1552489457364.jpg'
            ],

        };//Fim return
    },//Fim data

    computed:{
        randomImage(){
            return this.imagensLocais[this.randomIndex];
        },//fim randomImage

        randomImageInternet(){
            return this.imagensInternet[this.randomIndexInternet];
        }//fim randomImageInternet
    },//fim computed

    methods:{
        getRandomImage(){
            this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length)

            this.randomIndexInternet = Math.floor(Math.random()*this.imagensInternet.length)
        }//fim getRandomImage
    },//Fim methods

}).mount("#app");